<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/post', [App\Http\Controllers\PostController::class, 'index'])->middleware('can:view,App\Models\Post');
//Route::post('/post/create', [App\Http\Controllers\PostController::class, 'store'])->middleware('can:create,App\Models\Post');
//Route::get('post/gate', [App\Http\Controllers\PostController::class, 'gate'])->middleware('can:permistion'); // user gate

// not use middleware
Route::get('/post', [App\Http\Controllers\PostController::class, 'index']);
Route::post('/post/create', [App\Http\Controllers\PostController::class, 'store']);
Route::get('post/gate', [App\Http\Controllers\PostController::class, 'gate']);

