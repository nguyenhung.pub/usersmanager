<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/redirect', [App\Http\Controllers\Auth\LoginController::class, 'redirectToProvider'])->name("login.provider");
Route::get('/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleProviderCallback']);

Route::get('/post', [App\Http\Controllers\PostController::class, 'index']);
//Route::get('/post', [App\Http\Controllers\PostController::class, 'index'])->middleware('can:view,App\Models\Post');
//Route::post('/post/create', [App\Http\Controllers\PostController::class, 'store'])->middleware('can:create,App\Models\Post');
Route::post('/post/create', [App\Http\Controllers\PostController::class, 'store']);


Route::get('post/gate', [App\Http\Controllers\PostController::class, 'gate'])->middleware('can:permistion'); // user gate ok
